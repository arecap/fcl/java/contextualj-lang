/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.contextualj.lang.annotation;




import java.lang.annotation.*;

/**
 * Marker annotation.
 *
 *@{@link org.contextualj.lang.annotation.ContextPointcut} annotated targets are
 * subjects for a expression expression that matches join points. Context Oriented Bean and
 * Advice is associated with a pointcut expression and runs at any join point matched by the pointcut
 * (for example, the execution of a method with a certain name).
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Concern
@Documented
public @interface ContextPointcut {


}
