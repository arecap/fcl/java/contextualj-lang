/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.contextualj.lang.annotation.pointcut;


/**
 * Marker annotation.
 *
 * Method supported only
 *
 * @{@link org.contextualj.lang.annotation.pointcut.FinalBinding} annotated methods are
 * subject for a expression expression that matches join points Final Advice.
 * Final Advice are methods that could be defined in Context Oriented Beans.
 *
 * The Final Advice should be triggered regardless of the means by which a join point
 * exits (normal or exceptional return) process from the Context Bean method.
 * The Final Advice arguments are the {@link java.lang.Object} and the {@link java.lang.Throwable} that exists
 * from the normal execution of the process tree.
 * The Final Advice should return void and not be a {@link java.lang.Throwable}
 * The Final Advice stands for closing any I/Os, Commits, Rollbacks, etc of the Context Bean method execution.
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
import org.contextualj.lang.annotation.ContextPointcut;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ContextPointcut
@Documented
public @interface FinalBinding {
}
