/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.contextualj.lang.annotation.pointcut;


/**
 * Marker annotation.
 *
 * Method supported only
 *
 * @{@link org.contextualj.lang.annotation.pointcut.Bid} annotated methods are
 * subject for a expression expression that matches join points Bid Advice.
 * Bid Advice are methods that could be defined in Context Oriented Beans.
 *
 * The Bid Advice should be triggered before the Context Bean method to be called.
 * The Bid Advice arguments could be define as the Context Bean subject method
 * or intentionally not defined.
 * The Bid Advice should return void for a java bean mvc return changes
 * {@link org.contextualj.lang.annotation.pointcut.Wrap} is preferred.
 *
 * The Bid Advice could break the normal Context Bean method execution process if
 * {@link java.lang.Throwable} occurs.
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
import org.contextualj.lang.annotation.ContextPointcut;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ContextPointcut
@Documented
public @interface Bid {
}
