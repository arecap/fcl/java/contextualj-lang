/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.contextualj.lang.annotation.pointcut;



/**
 * Marker annotation.
 *
 * Method supported only
 *
 * @{@link org.contextualj.lang.annotation.pointcut.HandlingThrowing} annotated methods are
 * subject for a expression expression that matches join points Throwing Advice.
 * Throwing Advice are methods that could be defined in Context Oriented Beans.
 *
 * The Throwing Advice should be triggered when any process from the Context Bean method
 * oriented execution tree ends by throwing an exit value.
 * The Throwing Advice argument is the {@link java.lang.Throwable} that exits from
 * the normal execution of the process tree.
 * The Throwing Advice should return void and not be a {@link java.lang.Throwable}
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
import org.contextualj.lang.annotation.ContextPointcut;

import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ContextPointcut
@Documented
public @interface HandlingThrowing {

    Class<? extends Throwable>[] value() default {};

}
