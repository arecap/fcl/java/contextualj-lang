/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.contextualj.lang.weaving;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface InverseMultiplexer<I, R> {

    void bidMux(I i) throws Throwable;

    void returnMux(I i, R r) throws Throwable;

    void finalMux(I i, R r, Throwable throwable);

    void throwingMux(I i, Throwable throwable);

    R wrapTrackMux(I i) throws Throwable;

    default R inverseMultiplex(I i) throws Throwable {
        R r = null;
        Throwable throwable = null;
        try {
            bidMux(i);
            r = wrapTrackMux(i);
            returnMux(i, r);
        } catch (Throwable t) {
            throwable = t;
            throwingMux(i, throwable);
            throw throwable;
        } finally {
            finalMux(i, r, throwable);
        }
        return r;
    }

}
